<?phpinclude(_PS_MODULE_DIR_.'quickexport/quickexport.php');
class QuickExportTabName extends AdminTab{

	public function __construct(){
		$this->quickexport = new quickexport();
		return parent::__construct();
	}

	public function display(){
		$this->quickexport->token = $this->token;
		$this->quickexport->displayMain(); //in displayMain() you have to place html form and all html elements you want to display on your tab
	}
}
?>