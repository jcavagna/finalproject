<?php

if (!defined('_PS_VERSION_'))
  exit;

class QuickExport extends Module
{
	public function __construct(){
	    $this->name = 'quickexport';
	    $this->tab = 'export';

		$this->tabClassName = 'QuickExportTabName';
		$this->tabParentName = 'AdminTools';

	    $this->version = '0.1';
	    $this->author = 'Make it Joe';
	    $this->need_instance = 0;
	    $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.7');

	    parent::__construct();

	    $this->displayName = $this->l('Quickbooks Export Tool');
	    $this->description = $this->l('This module exports sales in a user-defined period to a .csv file to be imported into QuickBooks');

	    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

	    if (!Configuration::get('QUICKEXPORT_NAME'))
	      $this->warning = $this->l('No name provided');
  	}

	public function install(){
		if (!parent::install() ||
    		!$this->registerHook('DisplayBackOfficeHome') ||
			!Configuration::updateValue('QUICKEXPORT_NAME', 'Quick Export'))
			return false;
		return true;

		if (!$id_tab) {
			$tab = new Tab();
			$tab->class_name = $this->tabClassName;
			$tab->id_parent = Tab::getIdFromClassName($this->tabParentName);
			$tab->module = $this->name;
			$languages = Language::getLanguages();
			foreach ($languages as $language)
				$tab->name[$language['id_lang']] = $this->displayName;
				$tab->add();
		}
	}

	public function uninstall(){

		if (!parent::uninstall() ||
			!Configuration::deleteByName('QUICKEXPORT_NAME'))
			return false;
		return true;

		$id_tab = Tab::getIdFromClassName($this->tabClassName);
		if ($id_tab) {
			$tab = new Tab($id_tab);
			$tab->delete();
		}
	}

	public function getContent()
	{
	    $output = null;

	    if (Tools::isSubmit('submit'.$this->name))
	    {
	        $quick_export_name = strval(Tools::getValue('QUICKEXPORT_NAME'));
	        if (!$quick_export_name
	          || empty($quick_export_name)
	          || !Validate::isGenericName($quick_export_name))
	            $output .= $this->displayError($this->l('Invalid Configuration value'));
	        else
	        {
	            Configuration::updateValue('QUICKEXPORT_NAME', $quick_export_name);
	            $output .= $this->displayConfirmation($this->l('Settings updated'));
	        }
	    }
	    return $output.$this->displayForm();
	}

	public function hookDisplayBackOfficeHome(){
   		return $this->display(__FILE__, 'quickexport.tpl');
	}

	public function displayMain(){
		return $this->display(__FILE__, 'quickexport.tpl');
	}

	public function viewAccess($disable = false){
		$result = true;
		return $result;
	}

	public function displayForm()
	{
	    // Get default language
	    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

	    // Init Fields form array
	    $fields_form[0]['form'] = array(
	        'legend' => array(
	            'title' => $this->l('Settings'),
	        ),
	        'input' => array(
	            array(
	                'type' => 'text',
	                'label' => $this->l('Configuration value'),
	                'name' => 'QUICKEXPORT_NAME',
	                'size' => 20,
	                'required' => true
	            )
	        ),
	        'submit' => array(
	            'title' => $this->l('Save'),
	            'class' => 'button'
	        )
	    );

	    $helper = new HelperForm();

	    // Module, token and currentIndex
	    $helper->module = $this;
	    $helper->name_controller = $this->name;
	    $helper->token = Tools::getAdminTokenLite('AdminModules');
	    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

	    // Language
	    $helper->default_form_language = $default_lang;
	    $helper->allow_employee_form_lang = $default_lang;

	    // Title and toolbar
	    $helper->title = $this->displayName;
	    $helper->show_toolbar = true;        // false -> remove toolbar
	    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
	    $helper->submit_action = 'submit'.$this->name;
	    $helper->toolbar_btn = array(
	        'save' =>
	        array(
	            'desc' => $this->l('Save'),
	            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
	            '&token='.Tools::getAdminTokenLite('AdminModules'),
	        ),
	        'back' => array(
	            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
	            'desc' => $this->l('Back to list')
	        )
	    );

	    // Load current value
	    $helper->fields_value['QUICKEXPORT_NAME'] = Configuration::get('QUICKEXPORT_NAME');

	    return $helper->generateForm($fields_form);
	}
}