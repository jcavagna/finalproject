<?php /* Smarty version Smarty-3.1.14, created on 2014-07-18 21:32:38
         compiled from "/Users/Admin/Desktop/apserver/prestashop/modules/paypalusa/views/templates/front/express-checkout-messages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:97774561653c9cab69b4346-04653114%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1c0dfc87b3e1e08439dda0d8ddfc4b190e57ddeb' => 
    array (
      0 => '/Users/Admin/Desktop/apserver/prestashop/modules/paypalusa/views/templates/front/express-checkout-messages.tpl',
      1 => 1401835027,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '97774561653c9cab69b4346-04653114',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'paypal_usa_errors' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53c9cab6ae94f2_67310579',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53c9cab6ae94f2_67310579')) {function content_53c9cab6ae94f2_67310579($_smarty_tpl) {?><h1><?php echo smartyTranslate(array('s'=>'Unexpected payment error','mod'=>'paypalusa'),$_smarty_tpl);?>
</h1><?php if (isset($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_ERRORCODE0'])&&$_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_ERRORCODE0']==10486){?>	<div class="error">		<p><b><?php echo smartyTranslate(array('s'=>'Unfortunately, the payment was declined by PayPal for one of the following reasons:','mod'=>'paypalusa'),$_smarty_tpl);?>
</b></p>        <ul style="margin: 0px 0 15px 30px;">        	<li><?php echo smartyTranslate(array('s'=>'Billing address could not be confirmed','mod'=>'paypalusa'),$_smarty_tpl);?>
</li>            <li><?php echo smartyTranslate(array('s'=>'Transaction exceeds the card limit','mod'=>'paypalusa'),$_smarty_tpl);?>
</li>            <li><?php echo smartyTranslate(array('s'=>'Transaction denied by the card issuer','mod'=>'paypalusa'),$_smarty_tpl);?>
</li>        </ul>        <p><?php echo smartyTranslate(array('s'=>'Details:','mod'=>'paypalusa'),$_smarty_tpl);?>
            <?php echo smartyTranslate(array('s'=>'Short Error Message:','mod'=>'paypalusa'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_SHORTMESSAGE0'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<br />            <?php echo smartyTranslate(array('s'=>'Detailed Error Message:','mod'=>'paypalusa'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_LONGMESSAGE0'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<br />            <?php echo smartyTranslate(array('s'=>'Error Code:','mod'=>'paypalusa'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_ERRORCODE0'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
        </p>        <p><b><?php echo smartyTranslate(array('s'=>'Please click again on the PayPal Express Checkout button below and select another funding source on the PayPal website:','mod'=>'paypalusa'),$_smarty_tpl);?>
</b></p>        <?php echo $_smarty_tpl->getSubTemplate ('../hook/express-checkout.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paypal_usa_from_error'=>1), 0);?>
    </div><?php }else{ ?>    <p class="<?php if ($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['ACK']=='Failure'){?>error<?php }else{ ?>info<?php }?>">    	<b><?php echo smartyTranslate(array('s'=>'Unfortunately, an error occured while communicating with PayPal.','mod'=>'paypalusa'),$_smarty_tpl);?>
</b><br /><br />        <?php echo smartyTranslate(array('s'=>'Short Error Message:','mod'=>'paypalusa'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_SHORTMESSAGE0'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<br />        <?php echo smartyTranslate(array('s'=>'Detailed Error Message:','mod'=>'paypalusa'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_LONGMESSAGE0'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<br />        <?php echo smartyTranslate(array('s'=>'Error Code:','mod'=>'paypalusa'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['paypal_usa_errors']->value['L_ERRORCODE0'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<br />        <?php echo smartyTranslate(array('s'=>'Please','mod'=>'paypalusa'),$_smarty_tpl);?>
 <b><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact',true);?>
"><?php echo smartyTranslate(array('s'=>'contact our Customer service','mod'=>'paypalusa'),$_smarty_tpl);?>
</a></b> <?php echo smartyTranslate(array('s'=>'and mention this error code to get this issue resolved.','mod'=>'paypalusa'),$_smarty_tpl);?>
<br />    </p><?php }?><?php }} ?>